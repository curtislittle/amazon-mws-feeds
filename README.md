Amazon Marketplace Web Service Feeds PHP Client Library
=================================================

Installation
------------

Install [Composer](http://getcomposer.org/) and add amazon-mws-feeds to your `composer.json` by running this command:

    composer require curtislittle/amazon-mws-feeds

Version
-------

Amazon Marketplace Web Service Feeds PHP Client Library - Version 2009-01-01