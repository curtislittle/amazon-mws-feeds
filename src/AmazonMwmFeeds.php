<?php
/**
 * Created by PhpStorm.
 * User: clittle
 * Date: 10/22/2015
 * Time: 11:07 AM
 */

namespace CurtisLittle\AmazonMwsFeeds;

use MarketplaceWebService_Exception;
use MarketplaceWebService_Interface;

class AmazonMwsFeeds
{
    private $mwsFeedsClient;

    /**
     * AmazonMwsFeeds constructor.
     * @param MarketplaceWebService_Interface $mwsFeedsClient
     */
    public function __construct(MarketplaceWebService_Interface $mwsFeedsClient)
    {
        $this->mwsFeedsClient = $mwsFeedsClient;
    }


    // submit SubmitFeed operation
    // returns FeedSubmissionId
    // submit GetFeedSubmissionList operation
    // returns process complete yes/no
    // submit GetFeedSubmissionResult operation
    // returns processing report
    // if error free -> complete
    // else collect error and submit again
}